import axios from 'axios';
const url = 'http://localhost:8080/';
export default {
  files(formdata) {
    return axios
      .post(url + 'files',
            formdata,
            {headers: {
                'Content-Type': 'multipart/form-data'
            }})
      .then(response => response.data);
  }
};