import axios from 'axios';
const url = 'http://localhost:8080/';
export default {
  get_evaluations(user) {
    return axios
      .get(url + 'user/'+user+"/evaluations")
      .then(response => response.data);
  }
};