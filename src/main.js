import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/login.css'
import Notifications from 'vue-notification'

Vue.config.productionTip = false
Vue.use(Notifications)
// set auth header
Axios.defaults.headers.common['Authorization'] = `Bearer ${store.state.token}`;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
